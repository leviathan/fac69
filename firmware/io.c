#include "io.h"

// --------------------------------------------------------

extern uint32_t flashio_worker_begin;
extern uint32_t flashio_worker_end;

void set_flash_mode_spi()
{
    reg_spictrl = (reg_spictrl & ~0x007f0000) | 0x00000000;
}

void putchar(char c)
{
    reg_uart_data = c;
}

void print(const char *p)
{
    while (*p) {
        syn();
        putchar(*(p++));
        ack();
    }
    // NULL byte
    syn();
    putchar(0);
    ack();
}

char getchar_prompt(char *prompt)
{
    int32_t c = -1;

    uint32_t cycles_begin, cycles_now, cycles;
    __asm__ volatile ("rdcycle %0" : "=r"(cycles_begin));

    reg_leds = ~0;

    if (prompt)
        print(prompt);

    while (c == -1) {
        __asm__ volatile ("rdcycle %0" : "=r"(cycles_now));
        cycles = cycles_now - cycles_begin;
        if (cycles > 12000000) {
            if (prompt)
                print(prompt);
            cycles_begin = cycles_now;
            reg_leds = ~reg_leds;
        }
        c = reg_uart_data;
    }

    reg_leds = 0;
    return c;
}

char getchar()
{
    uint8_t c;
    // 3 is the termination character in ASCII
    c = (uint8_t)getchar_prompt(0);
    if(c==3) return 0;
    else return (char)c;
}

void get_uart_string(char *msgbuffer)
{
    char c;
    int i = 0;
    while(true) {
        ack();
        c = getchar();
        if(c) { 
            msgbuffer[i] = c;
            i++;
        }
        if(!c && i) {
            msgbuffer[i] = 0;
            return;
        }
    }
}

void ack()
{
    uint8_t c;
    while(true) {
        c = getchar();
        if(c==SYN) break;
    }
    putchar(ACK);
}

void syn()
{
    uint8_t c;
    putchar(SYN);
    while(true) {
        c = getchar();
        if(c==ACK) break;
    }
}

void leds(uint32_t val)
{
    reg_leds = val;
}
