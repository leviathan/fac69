#include "rnn.h"

/* 
 * Acces functions for the encoder layer
 */

#define MEM_SIZE(type) \
    ({ int MEM_SIZE = 0; switch(type){ \
         case LAYER_TYPE_ENCODER:           \
                MEM_SIZE = ENCODER_MEM_SIZE; break;      \
            case LAYER_TYPE_HIDDEN:       \
                MEM_SIZE = HIDDEN_MEM_SIZE; break;   \
            case LAYER_TYPE_DECODER:       \
                MEM_SIZE = DECODER_MEM_SIZE; break;   \
         }; MEM_SIZE; })

#define WEIGHT_COUNT(type) \
    ({ int WEIGHT_COUNT = 0; switch(type){ \
         case LAYER_TYPE_ENCODER:           \
                WEIGHT_COUNT = ENCODER_WEIGHT_COUNT; break;       \
            case LAYER_TYPE_HIDDEN:       \
                WEIGHT_COUNT = HIDDEN_WEIGHT_COUNT; break;   \
            case LAYER_TYPE_DECODER:       \
                WEIGHT_COUNT = DECODER_WEIGHT_COUNT; break;   \
         }; WEIGHT_COUNT; })

/*
 * Get access command
 */

#define ACCESS_CMD(type) \
    ({ int ACCESS_CMD = ACCESS_VALUES; switch(type){ \
         case LAYER_TYPE_ENCODER:           \
                ACCESS_CMD |= ENCODER; break;       \
            case LAYER_TYPE_HIDDEN:       \
                ACCESS_CMD |= HIDDEN; break;   \
            case LAYER_TYPE_DECODER:       \
                ACCESS_CMD |= DECODER; break;   \
         }; ACCESS_CMD; })

/* 
 * Generic function for reading neuron values:
 *
 * layer: what layer to access (encoder, decoder, hidden)
 * value_type: type (normal weight, RNN weight, bias)
 * nidx: Index of neuron
 * vidx: Index of value
 */
int get_layer_weight(
    enum layer_type layer,
    uint32_t nidx,
    uint32_t vidx
)
{
    if(layer==LAYER_TYPE_NONE) return 0;

    uint32_t addr = MEM_SIZE(layer)*nidx+vidx;
    ann_cmd = ACCESS_CMD(layer);
    ann_value1 = addr;
    return ann_value2;
}

/*
 * Function for reading bias
 */
int get_layer_bias(
    enum layer_type layer,
    uint32_t nidx
)
{
    return get_layer_weight(layer, nidx, WEIGHT_COUNT(layer));
}

/*
 * Function for reading learning rate
 */
int get_layer_alpha(
    enum layer_type layer,
    uint32_t nidx
)
{
    return get_layer_weight(layer, nidx, WEIGHT_COUNT(layer)+1);
}

/* 
 * Generic function for writing neuron values:
 *
 * layer: what layer to access (encoder, decoder, hidden)
 * value_type: type (normal weight, RNN weight, bias)
 * nidx: Index of neuron
 * vidx: Index of value
 * weight: The value
 */
void set_layer_weight(
    enum layer_type layer,
    uint32_t nidx,
    uint32_t vidx,
    int weight
)
{
    uint32_t addr = MEM_SIZE(layer)*nidx+vidx;
    ann_cmd = ACCESS_CMD(layer);
    ann_value1 = addr;
    ann_value2 = weight;
}

/*
 * Function for writing bias
 */
void set_layer_bias(
    enum layer_type layer,
    uint32_t nidx,
    int weight
)
{
    set_layer_weight(layer, nidx,  WEIGHT_COUNT(layer), weight);
}

/*
 * Function for writing learning rate
 */
void set_layer_alpha(
    enum layer_type layer,
    uint32_t nidx,
    int weight
)
{
    set_layer_weight(layer, nidx,  WEIGHT_COUNT(layer)+1, weight);
}

/*
 * Run inference
 */
uint16_t predict_next_token(uint16_t token)
{
    uint16_t ret;
    ann_cmd = RUN_INFERENCE|INFERENCE_START;
    ann_value1 = token;
    ann_cmd = RUN_INFERENCE|INFERENCE_FETCH;
    ret = ann_value1;
    return ret;
}

/*
 * Run trainingg
 */

/*
 * The mask is an XOR value of the bits of the supposed
 * output and what the network actually delivered
 */
uint16_t mask_back_propgatation(uint16_t mask)
{
    ann_cmd = RUN_TRAINING|TRAINING_SINGLE_SHOT;
    ann_value1 = mask;
}

/*
 * Access the LSFR output inside the ANN controller
 */
int get_random_value()
{
    int ret;
    ann_cmd = GENERATE_RANDOM_NUMBER;
    ret = ann_value1;
    return ret;
}

/*
 * Access the LSFR output inside the ANN controller
 * convert it to a char
 */
uint8_t get_random_char()
{
    int ret;
    ann_cmd = GENERATE_RANDOM_NUMBER;
    ret = ann_value1;
    return ret;
}

/*
 * Resetting the network: Clear LTSM
 */
void reset_network()
{
    ann_cmd = RESET_NETWORK;
}

/*
 * Setting the delta for the weights
 * We try to achieve something like a gradients
 */
void set_alpha(int alpha)
{
    int i, x, y;

    for(i=0;i<ENCODER_NEURON_COUNT;i++) {
        set_layer_alpha(LAYER_TYPE_ENCODER, i, alpha);
    }
    for(i=0;i<HIDDEN_NEURON_COUNT;i++) {
        set_layer_alpha(LAYER_TYPE_HIDDEN, i, alpha);
    }
    for(i=0;i<DECODER_NEURON_COUNT;i++) {
        set_layer_alpha(LAYER_TYPE_DECODER, i, alpha);
    }
}

/*
 * Initialize random weights
 */
void init_random_weights()
{
    int i, j, x, y;
    for(i=0;i<ENCODER_NEURON_COUNT;i++) {
        for(j=0;j<ENCODER_WEIGHT_COUNT;j++) {
            set_layer_weight(LAYER_TYPE_ENCODER,i, j, get_random_value());
        }
    }
    for(i=0;i<HIDDEN_NEURON_COUNT;i++) {
        for(j=0;j<HIDDEN_WEIGHT_COUNT;j++) {
            set_layer_weight(LAYER_TYPE_HIDDEN,i, j, get_random_value());
        }
    }
    for(i=0;i<DECODER_NEURON_COUNT;i++) {
        for(j=0;j<DECODER_WEIGHT_COUNT;j++) {
            set_layer_weight(LAYER_TYPE_DECODER,i, j, get_random_value());
        }
    }
}

/*
 * Set all biases to given value
 */
void set_bias_values(int bias)
{
    int i, x, y;
    for(i=0;i<ENCODER_NEURON_COUNT;i++) {
        set_layer_bias(LAYER_TYPE_ENCODER, i, bias);
    }
    for(i=0;i<HIDDEN_NEURON_COUNT;i++) {
        set_layer_bias(LAYER_TYPE_HIDDEN, i, bias);
    }
    for(i=0;i<DECODER_NEURON_COUNT;i++) {
        set_layer_bias(LAYER_TYPE_DECODER, i, bias);
    }
}

/*
 * Run training cycle for 1 epoch
 * epoch: The epoch we're in
 * learning_rate_zero: initial learning rate
 * decay_rate: the decay rate for gradient decay
 * xp: Pointer to array of values
 * xi: Index of y
 * 
 * Returns 0 when successful
 * Returns 1 when unsuccessful
 */
int run_training_single_epoch(
    int epoch,
    uint32_t learning_rate_zero,
    uint32_t decay_rate,
    uint16_t *xp,
    int xi
)
{
    int error;
    int last_val;
    uint16_t train_mask;
    uint16_t y = xp[xi-1];

    /*
     * Revert the LSTM states
     */
    reset_network();

    /*
     * Put all the prior tokens of the time series
     * into the LSTM for reaching the state we wish to train on
     */
    for(int xpi=0; xpi<xi-1; xpi++) {
        last_val = predict_next_token(xp[xpi]);
    }

    /*
     * Check whether the network has already been trained
     * Set return value to zero and exit the loop if so
     */
    train_mask = last_val ^ y;
    if(last_val==y) {
        return 0;
    }

    /*
     * Determine the training mask my finding out which
     * neurons misfired or didn't fire although they should have
     */
    set_alpha(learning_rate_zero/(1+(decay_rate*epoch)));
    mask_back_propgatation(train_mask);

    error = 0;
    for(int i=0;i<32;i++)
        error+=(train_mask>>i)&1;
    return error;
}


/*
 * Run training cycle for N epochs
 * num_epochs: Amount of epochs
 * learning_rate_zero: initial learning rate
 * decay_rate: the decay rate for gradient decay
 * xp: Pointer to array of values
 * xi: Index of y
 * 
 * Returns 0 when successful
 * Returns 1 when unsuccessful
 */
int run_training_epochs(
    int num_epochs,
    uint32_t learning_rate_zero,
    uint32_t decay_rate,
    uint16_t *xp,
    int xi
)
{
    int last_val;
    uint16_t train_mask;
    uint16_t y = xp[xi];;

    for(int epoch=0; epoch<num_epochs;epoch++) {
        if(run_training_single_epoch(
            epoch,
            learning_rate_zero,
            decay_rate,
            xp,
            xi
        )) return 0;
    }

    return 1;
}


/*
 * Run training cycle for N epochs
 * msgbuf: buffer for infos
 * num_epochs: Amount of epochs
 * learning_rate_zero: initial learning rate
 * decay_rate: the decay rate for gradient decay
 * xp: Pointer to array of values
 * xn: Amount of values in array
 */
char* run_training(
    char *msgbuf,
    int num_epochs,
    uint32_t learning_rate_zero,
    uint32_t decay_rate,
    uint16_t *xp,
    int xn
)
{
    int last_val;
    uint32_t train_mask;
    int ret;

    /*
     * Training the RNN for a time series
     * 1) Tax -> ation
     * 2) Tax -> ation -> is
     * 3) Tax -> ation -> is -> theft
     * 
     * xn is the amount of values we have in our token array
     * xi tells the functions where y is located in the array (x -> y)
     * xi must be xn-1 at most
     */
    for(int xi=1; xi<xn; xi++) {
        ret = run_training_epochs(
            num_epochs,
            learning_rate_zero,
            decay_rate,
            xp,
            xi
        );
        msgbuf = "FAIL";
        /*
         * When the training for the last time series failed
         * there's no point in training for a further token
         * in the time series.
         */
        if(!ret) break;
        /*
         * If we haven't terminated the loop by now, it means we're
         * still game
         */
        msgbuf = "SUCESS";
    }

    return msgbuf;
}
