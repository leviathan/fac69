/*
 *  PicoSoC - A simple example SoC using PicoRV32
 *
 *  Copyright (C) 2017  Claire Xenia Wolf <claire@yosyshq.com>
 *
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "rnn.h"
#include "io.h"

#define MEM_TOTAL 0x20000
#define MSGBUF 40
#define MAX_NUM_TOKENS 40

#define LRFACTOR 2000000000

// a pointer to this is a null pointer, but the compiler does not
// know that because "sram" is a linker symbol from sections.lds.
extern uint32_t sram;

void write_response(char *response)
{
    syn();
    ack();
    print(response);
}

void main()
{
    char msg[MSGBUF];
    char numstr[MSGBUF];
    char *response;
    
    int weight_idx;
    int neuron_idx;

    int weight_transferred;
    char weight_string[MSGBUF];

    leds(5);
    reg_uart_clkdiv = 104;
    leds(23);

    leds(5);
    set_flash_mode_spi();
    leds(23);

    enum modes {
      START,
      // Init mode
      INIT,
      // Read mode
      READ,
      READ_LAYER_NEURON,
      READ_LAYER_NEURON_VALUE,
      // Write mode
      WRITE,
      WRITE_LAYER,
      WRITE_LAYER_WEIGHTS,
      // Training the network
      TRAIN,
      TRAIN_STORE_TOKENS,
      TRAIN_STORE_LEARNING_RATE,
      TRAIN_STORE_DECAY_RATE,
      TRAIN_RUN_EPOCHS,
      TRAIN_RUN_SINGLE_EPOCH,
      // The fun part: Inference
      PREDICT
    } command_mode;
    command_mode = START;

    enum layer_type current_layer = LAYER_TYPE_NONE;
    enum layer_value_type current_layer_value = LAYER_VALUE_TYPE_NONE;
    uint32_t current_max_num_values;
    uint32_t current_num_neurons;

    // Write params
    uint32_t value_write_counter = 0;
    int new_value;

    // Training
    uint16_t new_token;
    uint16_t token_series[MAX_NUM_TOKENS];
    int token_counter = 0;
    uint32_t learning_rate = 0;
    uint32_t decay_rate = 0;
    uint32_t epoch_value = 0;

    while(true) {

        syn(); // Booted
        ack();

        for(int i=0;i<MSGBUF;i++) {
            msg[i]=0;
            numstr[i]=0;
            weight_string[i]=0;
        }
        get_uart_string(msg);
        strcpy(numstr, msg);

        if(!strcmp(msg,"DONE")) { // DONE always resets everything
            command_mode = START;
            write_response("OK");
            continue;
        }

        response = "ERROR";
        switch(command_mode) {
            case START:
                weight_idx = 0;
                neuron_idx = 0;
                response = "OK";
                if(!strcmp(msg,"HELLO")) {
                    response = "READY";
                }
                else if(!strcmp(msg,"INIT")) {
                    command_mode = INIT;
                }
                else if(!strcmp(msg,"READ")) {
                    command_mode = READ;
                }
                else if(!strcmp(msg,"WRITE")) {
                    command_mode = WRITE;
                }
                else if(!strcmp(msg,"RESET")) {
                    reset_network();
                    command_mode = START;
                }
                else if(!strcmp(msg,"TRAIN")) {
                    command_mode = TRAIN;
                }
                else if(!strcmp(msg,"PREDICT")) {
                    command_mode = PREDICT;
                }
                break;
            
            case INIT:
                command_mode = START;
                response = "OK";
                if(!strcmp(msg,"WEIGHTS")) {
                    init_random_weights();
                }
                else if(!strcmp(msg,"BIAS")) {
                    set_bias_values(1);
                }
                break;
            
            case READ:
                response = "OK";
                command_mode = READ_LAYER_NEURON;
                if(!strcmp(msg,"ENCODER")) {
                    response = "ENCODER";
                    current_layer = LAYER_TYPE_ENCODER;
                    current_num_neurons = ENCODER_NEURON_COUNT;
                    current_max_num_values = ENCODER_WEIGHT_COUNT;
                }
                else if(!strcmp(msg,"HIDDEN")) {
                    response = "HIDDEN";
                    current_layer = LAYER_TYPE_HIDDEN;
                    current_num_neurons = HIDDEN_NEURON_COUNT;
                    current_max_num_values = HIDDEN_WEIGHT_COUNT;
                }
                else if(!strcmp(msg,"DECODER")) {
                    response = "DECODER";
                    current_layer = LAYER_TYPE_DECODER;
                    current_num_neurons = DECODER_NEURON_COUNT;
                    current_max_num_values = DECODER_WEIGHT_COUNT;
                }
                break;

            case READ_LAYER_NEURON:
                neuron_idx = atoi(numstr);
                response = "END";
                command_mode = START;
                if(neuron_idx<current_num_neurons) {
                    response = "OK";
                    command_mode = READ_LAYER_NEURON_VALUE;
                }
                break;

            case READ_LAYER_NEURON_VALUE:
                response = "ERROR";
                if(!strcmp(msg,"BIAS")) {
                    weight_transferred = get_layer_bias(current_layer, neuron_idx);
                    response = itoa(weight_transferred, numstr, 10);
                }
                else {
                    weight_idx = atoi(numstr);
                    response = "END";
                    if(weight_idx<current_max_num_values) {
                        weight_transferred = get_layer_weight(current_layer, neuron_idx, weight_idx);
                        response = itoa(weight_transferred, numstr, 10);
                    }
                }
                break;

            case WRITE:
                response = "OK";
                command_mode = WRITE_LAYER;
                if(!strcmp(msg,"ENCODER")) {
                    current_layer = LAYER_TYPE_ENCODER;
                    current_num_neurons = ENCODER_NEURON_COUNT;
                    current_max_num_values = ENCODER_WEIGHT_COUNT;
                }
                else if(!strcmp(msg,"HIDDEN")) {
                    current_layer = LAYER_TYPE_HIDDEN;
                    current_num_neurons = HIDDEN_NEURON_COUNT;
                    current_max_num_values = HIDDEN_WEIGHT_COUNT;
                }
                else if(!strcmp(msg,"DECODER")) {
                    current_layer = LAYER_TYPE_DECODER;
                    current_num_neurons = DECODER_NEURON_COUNT;
                    current_max_num_values = DECODER_WEIGHT_COUNT;
                }
                break;

            case WRITE_LAYER:
                response = "OK";
                if(!strcmp(msg,"WEIGHTS")) {
                    command_mode = WRITE_LAYER_WEIGHTS;
                    current_layer_value = LAYER_VALUE_TYPE_WEIGHTS;
                    value_write_counter = 0;
                }
                else if(!strcmp(msg,"BIAS")) {
                    command_mode = WRITE_LAYER_WEIGHTS;
                    current_layer_value = LAYER_VALUE_TYPE_BIAS;
                    current_max_num_values = 1;
                    value_write_counter = 0;
                }
                else {
                    neuron_idx = atoi(numstr);
                    response = (neuron_idx<current_num_neurons)?msg:"END";
                    value_write_counter = 0;
                }
                break;

            case WRITE_LAYER_WEIGHTS:
                response = "END";
                if((value_write_counter<current_max_num_values) && (current_layer_value == LAYER_VALUE_TYPE_WEIGHTS) ) {
                    response = numstr;
                    new_value = atoi(numstr);
                    set_layer_weight(current_layer, neuron_idx, value_write_counter, new_value);
                    value_write_counter++;
                } else if (current_layer_value == LAYER_VALUE_TYPE_BIAS) {
                    response = numstr;
                    new_value = atoi(numstr);
                    set_layer_bias(current_layer, neuron_idx, new_value);
                }
                break;

            case TRAIN:
                if(!strcmp(msg,"TOKENS")) {
                    command_mode = TRAIN_STORE_TOKENS;
                    response = "OK";
                    token_counter = 0;
                }
                else if(!strcmp(msg,"LEARNING_RATE")) {
                    response = "OK";
                    command_mode = TRAIN_STORE_LEARNING_RATE;
                }
                else if(!strcmp(msg,"DECAY_RATE")) {
                    response = "OK";
                    command_mode = TRAIN_STORE_DECAY_RATE;
                }
                else if(!strcmp(msg,"RUN_EPOCHS")) {
                    command_mode = TRAIN_RUN_EPOCHS;
                    response = "OK";
                }
                else if(!strcmp(msg,"RUN_SINGLE_EPOCH")) {
                    command_mode = TRAIN_RUN_SINGLE_EPOCH;
                    response = "OK";
                }
                break;

            case TRAIN_STORE_TOKENS:
                if(token_counter<MAX_NUM_TOKENS) {
                    new_token = atoi(numstr);
                    token_series[token_counter] = new_token;
                    token_counter++;
                    response = itoa(token_counter, numstr, 10);
                } else {
                    response = "END";
                }
                break;

            case TRAIN_STORE_LEARNING_RATE:
                learning_rate = atoi(numstr);
                response = numstr;
                command_mode = START;
                break;

            case TRAIN_STORE_DECAY_RATE:
                decay_rate = atoi(numstr);
                response = numstr;
                command_mode = START;
                break;

            case TRAIN_RUN_EPOCHS:
                epoch_value = atoi(numstr);
                response = run_training(response, epoch_value, learning_rate, decay_rate, token_series, token_counter);
                command_mode = START;
                break;

            case TRAIN_RUN_SINGLE_EPOCH:
                epoch_value = atoi(numstr);
                new_value = run_training_single_epoch(epoch_value, learning_rate, decay_rate, token_series, token_counter);
                response = itoa(new_value, numstr, 10);
                break;

            case PREDICT:
                new_token = atoi(numstr);
                for(int i=0;i<MSGBUF;i++) {
                    numstr[i]=0;
                }
                new_token = predict_next_token(new_token);
                response = itoa(new_token, numstr, 10);
                break;

        }
        write_response(response);
    }
}
