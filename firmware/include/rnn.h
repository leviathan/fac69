#include <stdint.h>
#include <stdbool.h>

#include "defines.h"

#define ann_cmd (*(volatile uint32_t*)0x04000000)
#define ann_value1 (*(volatile uint32_t*)0x04000004)
#define ann_value2 (*(volatile uint32_t*)0x04000008)

#define ACCESS_VALUES 1
#define RUN_INFERENCE 2
#define RUN_TRAINING 3
#define GENERATE_RANDOM_NUMBER 4
#define RESET_NETWORK 5

#define ENCODER (1<<8)
#define HIDDEN (2<<8)
#define DECODER (3<<8)

#define INFERENCE_START (1<<8)
#define INFERENCE_FETCH (2<<8)

#define TRAINING_SINGLE_SHOT (1<<8)

//#define ENCODER_WEIGHT_COUNT (NUM_INPUT_SYNAPSES+NUM_INPUT_NEURONS)
#define ENCODER_WEIGHT_COUNT (NUM_INPUT_SYNAPSES)
#define HIDDEN_WEIGHT_COUNT (NUM_INPUT_NEURONS+NUM_HIDDEN_NEURONS_H)
#define DECODER_WEIGHT_COUNT (NUM_HIDDEN_NEURONS_H)

#define ENCODER_MEM_SIZE (ENCODER_WEIGHT_COUNT+2)
#define HIDDEN_MEM_SIZE (HIDDEN_WEIGHT_COUNT+2)
#define DECODER_MEM_SIZE (DECODER_WEIGHT_COUNT+2)

#define ENCODER_NEURON_COUNT NUM_INPUT_NEURONS
#define HIDDEN_NEURON_COUNT (NUM_HIDDEN_NEURONS_W*NUM_HIDDEN_NEURONS_H)
#define DECODER_NEURON_COUNT NUM_OUTPUT_NEURONS

enum layer_type {
    LAYER_TYPE_ENCODER,
    LAYER_TYPE_HIDDEN,
    LAYER_TYPE_DECODER,
    LAYER_TYPE_NONE
};

enum layer_value_type {
    LAYER_VALUE_TYPE_WEIGHTS,
    LAYER_VALUE_TYPE_BIAS,
    LAYER_VALUE_TYPE_DELTA_W,
    LAYER_VALUE_TYPE_NONE
};

/* 
 * layer: what layer to access (encoder, decoder, hidden)
 * value_type: type (normal weight, RNN weight, bias)
 * nidx: Index of neuron
 * vidx: Index of value
 */
int get_layer_weight(
    enum layer_type layer,
    uint32_t nidx,
    uint32_t vidx
);

/*
 * Function for reading bias
 */
int get_layer_bias(
    enum layer_type layer,
    uint32_t nidx
);

/*
 * Function for reading learning rate
 */
int get_layer_alpha(
    enum layer_type layer,
    uint32_t nidx
);

/* 
 * Generic function for writing neuron values:
 *
 * layer: what layer to access (encoder, decoder, hidden)
 * value_type: type (normal weight, RNN weight, bias)
 * nidx: Index of neuron
 * vidx: Index of value
 * weight: The value
 */
void set_layer_weight(
    enum layer_type layer,
    uint32_t nidx,
    uint32_t vidx,
    int weight
);

/*
 * Function for writing bias
 */
void set_layer_bias(
    enum layer_type layer,
    uint32_t nidx,
    int weight
);

/*
 * Function for writing learning rate
 */
void set_layer_alpha(
    enum layer_type layer,
    uint32_t nidx,
    int weight
);

/*
 * Run inference
 */
uint16_t predict_next_token(uint16_t token);

/*
 * Run trainingg
 */

/*
 * The mask is an XOR value of the bits of the supposed
 * output and what the network actually delivered
 */
uint16_t mask_back_propgatation(uint16_t mask);

/*
 * Access the LSFR output inside the ANN controller
 */
int get_random_value();

/*
 * Access the LSFR output inside the ANN controller
 * convert it to a char
 */
uint8_t get_random_char();

/*
 * Initialize random weights
 */
void init_random_weights();

/*
 * Set all biases to given value
 */
void set_bias_values(int bias);

/*
 * Resetting the network: Clear LTSM
 */
void reset_network();

/*
 * Run training cycle for N epochs
 * msgbuf: buffer for infos
 * num_epochs: Amount of epochs
 * learning_rate_zero: initial learning rate
 * decay_rate: the decay rate for gradient decay
 * xp: Pointer to array of values
 * xn: Amount of values in array
 */
char* run_training(
    char *msgbuf,
    int num_epochs,
    uint32_t learning_rate_zero,
    uint32_t decay_rate,
    uint16_t *xp,
    int xi
);

/*
 * Run training cycle for 1 epoch
 * epoch: The epoch we're in
 * learning_rate_zero: initial learning rate
 * decay_rate: the decay rate for gradient decay
 * xp: Pointer to array of values
 * xi: Index of y
 * 
 * Returns 0 when successful
 * Returns 1 when unsuccessful
 */
int run_training_single_epoch(
    int epoch,
    uint32_t learning_rate_zero,
    uint32_t decay_rate,
    uint16_t *xp,
    int xi
);
