#include <stdint.h>
#include <stdbool.h>

#define reg_spictrl (*(volatile uint32_t*)0x02000000)
#define reg_uart_clkdiv (*(volatile uint32_t*)0x02000004)
#define reg_uart_data (*(volatile uint32_t*)0x02000008)
#define reg_leds (*(volatile uint32_t*)0x03000000)

#define ACK 06
#define SYN 22

void set_flash_mode_spi();

void putchar(char c);

void print(const char *p);

char getchar();

void ack();

void syn();

void leds(uint32_t val);

void get_uart_string(char *msg);
