# Parameters of network:

NETWORK_PARAMS = \
	NUM_INPUT_SYNAPSES:16 \
	NUM_INPUT_NEURONS:3 \
	NUM_OUTPUT_NEURONS:16 \
	NUM_HIDDEN_NEURONS_W:2 \
	NUM_HIDDEN_NEURONS_H:2

#NETWORK_PARAMS = \
	NUM_INPUT_SYNAPSES:4 \
	NUM_INPUT_NEURONS:3 \
	NUM_OUTPUT_NEURONS:4 \
	NUM_HIDDEN_NEURONS_W:2 \
	NUM_HIDDEN_NEURONS_H:2

TARGET_BOARD?=ecp5_minifpga

CICD_VERILATOR_CFLAGS?=

BENCHES=

VERILATOR_DIR?=/usr/share/verilator/include

VERILATOR_CFLAGS=-I$(VERILATOR_DIR) -Iobj_dir -std=c++17 -fcoroutines $(DFLAGS)

VERILATOR=verilator --trace --timing  -Wno-lint --vpi \
	--timescale 1ns/1ps \
	-Isrc/sim \
	-Isubmodules/picorv32 \
	-Isubmodules/picorv32/picosoc

ifeq ($(TARGET_BOARD), ecp5_minifpga)
	NEXTPNR=nextpnr-ecp5
	TARGET=ecp5
	BENCHES+=testbench_ecp5_minifpga.bin
	CFGDIR=configs/ecp5
	LPFILE=configs/ecp5/minifpga.lpf
endif

VERILATOR_FILES= \
$(VERILATOR_DIR)/verilated.cpp \
$(VERILATOR_DIR)/verilated_vpi.cpp \
$(VERILATOR_DIR)/verilated_vcd_c.cpp \
$(VERILATOR_DIR)/verilated_threads.cpp \
$(VERILATOR_DIR)/verilated_timing.cpp \
src/cpptb/telnet_server.cpp \
src/cpptb/telnet_session.cpp

all: result/firmware.hex $(BENCHES)

src/rtl/params.vh:
	@ $(foreach PARAMS,$(NETWORK_PARAMS), \
		$(eval PARAM = $(word 1,$(subst :, ,$(PARAMS)))) \
		$(eval VALUE = $(word 2,$(subst :, ,$(PARAMS)))) \
		\
		echo "\`define $(PARAM) $(VALUE)" >> $@ ; \
	)

src/cpptb/params.h:
	@ $(foreach PARAMS,$(NETWORK_PARAMS), \
		$(eval PARAM = $(word 1,$(subst :, ,$(PARAMS)))) \
		$(eval VALUE = $(word 2,$(subst :, ,$(PARAMS)))) \
		\
		echo "#define $(PARAM) $(VALUE)" >> $@ ; \
	)

firmware/include/defines.h:
	@ $(foreach PARAMS,$(NETWORK_PARAMS), \
		$(eval PARAM = $(word 1,$(subst :, ,$(PARAMS)))) \
		$(eval VALUE = $(word 2,$(subst :, ,$(PARAMS)))) \
		\
		echo "#define $(PARAM) $(VALUE)" >> $@ ; \
	)

testbench_%.bin: obj_dir/V%__ALL.a src/cpptb/%.cpp src/cpptb/params.h
	$(CXX) $(VERILATOR_CFLAGS) $(VERILATOR_FILES) src/cpptb/$*.cpp obj_dir/V$*__ALL.a -o $@
	rm -rf obj_dir

obj_dir/V%__ALL.a: src/rtl/%.v src/rtl/params.vh
	$(VERILATOR) $(CICD_VERILATOR_CFLAGS) --top-module $* -cc src/rtl/$*.v -Isrc/rtl
	make -C obj_dir -f V$*.mk

result/firmware.hex: result firmware/include/defines.h
	make -C firmware firmware.hex
	cp firmware/firmware.hex firmware.hex
	cp firmware/firmware.hex result/firmware.hex

result:
	mkdir -p result
clean:
	rm -rf 	*.bin *.vcd obj_dir src/cpptb/*.o soc.json src/rtl/params.vh \
	src/cpptb/params.h firmware/include/defines.h result/firmware.hex
	make -C firmware clean
