/*
In genereal the activation function is:
             -----
             \
              \  (w_i*x_i)+bias
f(w_i,x_i) =  /
             /____

when f(w,x) > 0, then the output spikes

*/
module neuron #(
    parameter NUMBER_SYNAPSES = 8
)
(
    clk,
    rst,
    // Turn on processing
    run_inference,
    inference_done,
    // The layer i/o
    neuron_inputs,
    neuron_output,
    // Backprop
    backprop_enable,
    backprop_out_port,
    backprop_in_port,
    backprop_done,
    // Data interface
    read_enable,
    write_enable,
    read_done,
    write_done,
    address,
    data_i,
    data_o
);
    input clk;
    input rst;
    input run_inference;
    output reg inference_done;
    input[NUMBER_SYNAPSES-1:0] neuron_inputs;
    output reg neuron_output;

    output reg write_done;
    output reg read_done;

    // Data interface
    input write_enable;
    input read_enable;
    input [$clog2(NUMBER_SYNAPSES+2)+1:0] address;
    input [31:0] data_i;
    output reg [31:0] data_o;

    // Backprop
    output reg [NUMBER_SYNAPSES-1:0] backprop_out_port; // tell the other neurons that they have to better themselves
    input backprop_in_port; // Constructive criticism for the neuron
    output reg backprop_done;
    input backprop_enable;
    reg backprop_running;

    reg signed[NUMBER_SYNAPSES-1:0][31:0] W; // summands
    reg signed[31:0] bias; // summands
    reg signed[31:0] dw; // weight correction

    genvar gv;
    integer regidx;

    reg signed [31:0] Y;

    always @(posedge clk)
    begin

        if (rst)
        begin
            neuron_output <= 0;
            regidx <= 0;
            Y <= $signed(bias);
            data_o <= 0;
            backprop_done <= 0;
            inference_done <= 0;
            backprop_running <= 0;
            write_done <= 0;
            read_done <= 0;
        end

        else
        if(backprop_done && !backprop_enable ) begin
            backprop_done <= 0;
        end

        else
        if(!backprop_done && backprop_running)
        begin
            // Adjust normal weights
            if( regidx < NUMBER_SYNAPSES )
            begin
                if( backprop_in_port && neuron_output && neuron_inputs[regidx] ) begin
                    W[regidx] <= $signed(W[regidx]) - $signed(dw);
                end
                else
                if( backprop_in_port && !neuron_output && neuron_inputs[regidx] ) begin
                    W[regidx] <= $signed(W[regidx]) + $signed(dw);
                end
                backprop_out_port[regidx] <= backprop_in_port ^ neuron_inputs[regidx];
                regidx <= regidx+1;
            end
            // Reset the redister index counter
            else if( regidx == NUMBER_SYNAPSES )
            begin
                //$display("Backprop done");
                backprop_done <= 1;
                backprop_running <= 0;
                regidx <= 0;
            end
        end

        else
        if( !backprop_done && backprop_enable && !run_inference )
        begin
            backprop_out_port <= 0;
            backprop_running <= 1;
            regidx <= 0;
        end

        else
        if( !run_inference && !write_done && write_enable ) // Data access process
        begin
            if(address<NUMBER_SYNAPSES) begin
                W[address] <= data_i;
            end
            else
            if(address<(NUMBER_SYNAPSES+1)) begin
                bias <= data_i;
            end
            else
            if(address<(NUMBER_SYNAPSES+2)) begin
                dw <= data_i;
            end
            write_done <= 1;
        end

        else
        if( !run_inference && !read_done && read_enable ) // Data access process
        begin
            if(address<NUMBER_SYNAPSES) begin
                data_o <= W[address];
            end
            else
            if(address<(NUMBER_SYNAPSES+1)) begin
                data_o <= bias;
            end
            else
            if(address<(NUMBER_SYNAPSES+2)) begin
                data_o <= dw;
            end
            read_done <= 1;
        end

        else
        if(run_inference && !inference_done)
        begin

            if( regidx < NUMBER_SYNAPSES )
            begin
                Y <= $signed(Y) + $signed((neuron_inputs[regidx]==1'b1) ? W[regidx] : 0);
                regidx <= regidx+1;
            end

            else if( regidx == NUMBER_SYNAPSES )
            begin
                regidx <= 0;
                neuron_output <= ($signed(Y)>0);
                Y <= $signed(bias);
                inference_done <= 1;
            end
        end

        else

        // House keeping
        if(!read_enable && read_done ) read_done <= 0;
        if(!run_inference && inference_done) inference_done <= 0;
        if(!write_enable && write_done) write_done <= 0;

    end

endmodule
