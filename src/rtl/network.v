`include "params.vh"

module network
(
    clk,
    rst,
    // DMA interface
    mem_enable,
    mem_ready,
    mem_wstrb,
    mem_addr,
    mem_data_i,
    mem_data_o
);
    input clk;
    input rst;
    
    // Turn on processing
    wire run_inference;
    wire rst_ann;

    // The layer i/o
    wire[`NUM_INPUT_SYNAPSES-1:0] layer_inputs;
    wire[`NUM_OUTPUT_NEURONS-1:0] layer_outputs;

    // DMA interface
    input mem_enable;
    input [3:0]  mem_wstrb;
    input [23:0] mem_addr;
    input [31:0] mem_data_i;
    output[31:0] mem_data_o;
    output mem_ready;

    // Encoder
    wire[`NUM_INPUT_NEURONS-1:0] encoder_outputs;
    wire [31:0] encoder_addr;
    wire [31:0] encoder_data_i;
    wire [31:0] encoder_data_o;
    wire encoder_backprop_done;
    wire encoder_inference_done;
    wire encoder_we;
    wire encoder_read_enable;
    wire encoder_read_done;
    wire encoder_write_done;

    // Hidden
    wire [31:0] hidden_addr;
    wire [31:0] hidden_data_i;
    wire [31:0] hidden_data_o;
    wire hidden_backprop_done;
    wire hidden_inference_done;
    wire hidden_we;
    wire hidden_read_enable;
    wire hidden_read_done;
    wire hidden_write_done;
    
    wire[`NUM_HIDDEN_NEURONS_H-1:0] hidden_outputs;

    // Decoder
    wire [31:0] decoder_addr;
    wire [31:0] decoder_data_i;
    wire [31:0] decoder_data_o;
    wire decoder_backprop_done;
    wire decoder_inference_done;
    wire decoder_we;
    wire decoder_read_enable;
    wire decoder_read_done;
    wire decoder_write_done;

    // Training interface
    wire[`NUM_OUTPUT_NEURONS-1:0] backprop_port; // tell the other neurons that they have to better themselves
    wire[`NUM_HIDDEN_NEURONS_H-1:0] hidden_backprop_port; // tell the other neurons that they have to better themselves
    wire[`NUM_INPUT_NEURONS-1:0] encoder_backprop_port; // tell the other neurons that they have to better themselves
    
    wire backprop_enable;

    network_controller #(
        .NUMBER_SYNAPSES(`NUM_INPUT_SYNAPSES),
        .NUMBER_INPUT_NEURONS(`NUM_INPUT_NEURONS),
        .NUMBER_OUTPUT_NEURONS(`NUM_OUTPUT_NEURONS),
        .NUMBER_HIDDEN_NEURONS_W(`NUM_HIDDEN_NEURONS_W),
        .NUMBER_HIDDEN_NEURONS_H(`NUM_HIDDEN_NEURONS_H)
    ) controller (
        .clk(clk),
        .rst(rst),
        .rst_ann(rst_ann),
        // Turn on processing
        .run_inference(run_inference),
        .decoder_inference_done(decoder_inference_done),
        .encoder_inference_done(encoder_inference_done),
        // DMA interface
        .mem_enable(mem_enable),
        .mem_wstrb(mem_wstrb),
        .mem_addr(mem_addr),
        .mem_data_i(mem_data_i),
        .mem_data_o(mem_data_o),
        .mem_ready(mem_ready),
        // Layer I/O
        .encoder_input(layer_inputs),
        .encoder_output(encoder_outputs),
        .decoder_output(layer_outputs),
        // The training interface
        .backprop_port(backprop_port),
        .backprop_done(decoder_backprop_done && hidden_backprop_done && encoder_backprop_done),
        .backprop_enable(backprop_enable),
        // Encoder
        .encoder_addr(encoder_addr),
        .encoder_data_i(encoder_data_i),
        .encoder_data_o(encoder_data_o),
        .encoder_we(encoder_we),
        .encoder_read_enable(encoder_read_enable),
        .encoder_read_done(encoder_read_done),
        .encoder_write_done(encoder_write_done),
        // Hidden
        .hidden_addr(hidden_addr),
        .hidden_data_i(hidden_data_i),
        .hidden_data_o(hidden_data_o),
        .hidden_we(hidden_we),
        .hidden_read_enable(hidden_read_enable),
        .hidden_read_done(hidden_read_done),
        .hidden_write_done(hidden_write_done),
        // Decoder
        .decoder_addr(decoder_addr),
        .decoder_data_i(decoder_data_i),
        .decoder_data_o(decoder_data_o),
        .decoder_we(decoder_we),
        .decoder_read_enable(decoder_read_enable),
        .decoder_read_done(decoder_read_done),
        .decoder_write_done(decoder_write_done)
    );

    // Encoder
    layer #(
        .NUMBER_SYNAPSES(`NUM_INPUT_SYNAPSES),
        .NUMBER_NEURONS(`NUM_INPUT_NEURONS),
        .IS_RNN(0)
    )
    encoder(
        .clk(clk),
        .rst(rst_ann),
        .run_inference(run_inference),
        .inference_done(encoder_inference_done),
        .layer_inputs(layer_inputs),
        .layer_outputs(encoder_outputs),
        // The training interface
        .backprop_out_port(),
        .backprop_in_port(encoder_backprop_port),
        .backprop_done(encoder_backprop_done),
        .backprop_enable(backprop_enable),
        // Data interface
        .write_enable(encoder_we),
        .read_enable(encoder_read_enable),
        .read_done(encoder_read_done),
        .write_done(encoder_write_done),
        .address(encoder_addr),
        .data_i(encoder_data_o),
        .data_o(encoder_data_i)
    );

    // Hidden Layers

    neuron_matrix #(
        .NUMBER_SYNAPSES(`NUM_INPUT_NEURONS),
        .NUMBER_NEURONS_W(`NUM_HIDDEN_NEURONS_W),
        .NUMBER_NEURONS_H(`NUM_HIDDEN_NEURONS_H)
    )
    hidden_layers(
        .clk(clk),
        .rst(rst_ann),
        .run_inference(encoder_inference_done),
        .inference_done(hidden_inference_done),
        .layer_inputs(encoder_outputs),
        .layer_outputs(hidden_outputs),
        // The training interface
        .backprop_out_port(encoder_backprop_port),
        .backprop_in_port(hidden_backprop_port),
        .backprop_done(hidden_backprop_done),
        .backprop_enable(backprop_enable),
        // Data interface
        .write_enable(hidden_we),
        .read_enable(hidden_read_enable),
        .read_done(hidden_read_done),
        .write_done(hidden_write_done),
        .address(hidden_addr),
        .data_i(hidden_data_o),
        .data_o(hidden_data_i)
    );

    // Decoder
    layer #(
        .NUMBER_SYNAPSES(`NUM_HIDDEN_NEURONS_H),
        .NUMBER_NEURONS(`NUM_OUTPUT_NEURONS),
        .IS_RNN(0)
    )
    decoder(
        .clk(clk),
        .rst(rst_ann),
        .run_inference(hidden_inference_done),
        .inference_done(decoder_inference_done),
        .layer_inputs(hidden_outputs),
        .layer_outputs(layer_outputs),
        // The training interface
        .backprop_out_port(hidden_backprop_port),
        .backprop_in_port(backprop_port),
        .backprop_done(decoder_backprop_done),
        .backprop_enable(backprop_enable),
        // Data interface
        .write_enable(decoder_we),
        .read_enable(decoder_read_enable),
        .read_done(decoder_read_done),
        .write_done(decoder_write_done),
        .address(decoder_addr),
        .data_i(decoder_data_o),
        .data_o(decoder_data_i)
    );

endmodule
