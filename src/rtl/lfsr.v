module lfsr(
    clk,
    rst,
    op
);
    input clk;
    input rst;
    output reg [31:0] op;

    always@(posedge clk) begin
        if(rst) begin
            op <= 8'hf;
        end
        else
        begin
            op <= (op << 1);
            op[0] <= op[31] ^ op[29] ^ op[25] ^ op[24];
        end
    end
endmodule
