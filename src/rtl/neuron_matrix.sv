module neuron_matrix #(
    parameter NUMBER_SYNAPSES = 5,
    parameter NUMBER_NEURONS_W = 5,
    parameter NUMBER_NEURONS_H = 5
)
(
    clk,
    rst,
    // Turn on processing
    run_inference,
    inference_done,
    // The layer i/o
    layer_inputs,
    layer_outputs,
    // Backprop
    backprop_out_port,
    backprop_in_port,
    backprop_done,
    backprop_enable,
    // Data interface
    write_enable,
    read_enable,
    read_done,
    write_done,
    address,
    data_i,
    data_o
);
    parameter values_neuron = NUMBER_SYNAPSES+NUMBER_NEURONS_H+2;
    parameter values_layer = NUMBER_NEURONS_H*values_neuron;
    parameter total_neurons = NUMBER_NEURONS_W*values_layer;

    input clk;
    input rst;
    input run_inference;
    output inference_done;
    output backprop_done;
    input backprop_enable;

    input[NUMBER_SYNAPSES-1:0] layer_inputs;
    output[NUMBER_NEURONS_H-1:0] layer_outputs;

    // Data interface
    input write_enable;
    input read_enable;
    output reg write_done;
    output reg read_done;
    input [$clog2(total_neurons+1):0] address;
    input [31:0] data_i;
    output reg[31:0] data_o;

    // Backprop
    output[NUMBER_SYNAPSES-1:0] backprop_out_port; // tell the other neurons that they have to better themselves
    input[NUMBER_NEURONS_H-1:0] backprop_in_port; // Constructive criticism for the neurons

    wire[NUMBER_NEURONS_W-1:0][NUMBER_NEURONS_H-1:0] backprop_ports; // Constructive criticism

    // wires for wiring the network together
    wire [NUMBER_NEURONS_W-1:0][$clog2(values_layer):0]translated_address;
    generate
        for(genvar gv=0;gv<NUMBER_NEURONS_W;gv=gv+1) begin : translate_addr_m
            assign translated_address[gv] = address-gv*values_layer;
        end
    endgenerate

    wire[NUMBER_NEURONS_W-1:0][NUMBER_NEURONS_H-1:0] layer_output_array;

    wire [NUMBER_NEURONS_W-1:0] read_enable_array;
    generate
        for(genvar gv=0;gv<NUMBER_NEURONS_W;gv=gv+1) begin : re_arr_m
            assign read_enable_array[gv] = (translated_address[gv]<values_layer) && read_enable;
        end
    endgenerate

    wire [NUMBER_NEURONS_W-1:0] write_enable_array;
    generate
        for(genvar gv=0;gv<NUMBER_NEURONS_W;gv=gv+1) begin : rw_arr_m
            assign write_enable_array[gv] = (translated_address[gv]<values_layer) && write_enable;
        end
    endgenerate

    wire[NUMBER_NEURONS_W-1:0] inference_done_array;
    wire[NUMBER_NEURONS_W-1:0] backprop_done_array;

    wire [NUMBER_NEURONS_W-1:0][31:0] data_o_array;
    wire [NUMBER_NEURONS_W-1:0] read_done_array;
    wire [NUMBER_NEURONS_W-1:0] write_done_array;

    always @(posedge clk) begin
        if(rst) begin
            data_o <= 0;
            read_done <= 0;
        end
        else begin
            for (int i=0; i<NUMBER_NEURONS_W; i+=1) begin
                if(write_enable_array[i]) begin
                    write_done <= write_done_array[i];
                end
                if(read_enable_array[i]) begin
                    data_o <= data_o_array[i];
                    read_done <= read_done_array[i];
                end
            end
        end
        if(read_done && !read_enable) read_done <= 0;
        if(write_done && !write_enable) write_done <= 0;
    end

    generate
        for(genvar gv=0;gv<NUMBER_NEURONS_W;gv=gv+1) begin : matrix_layers
            layer #(
                .NUMBER_SYNAPSES(NUMBER_SYNAPSES),
                .NUMBER_NEURONS(NUMBER_NEURONS_H),
                .IS_RNN(1)
            )
            hidden_layer(
                .clk(clk),
                .rst(rst),
                // Enable inference
                .run_inference((gv==0)?run_inference:inference_done_array[gv-1]),
                .inference_done(inference_done_array[gv]),
                // Data IO
                .layer_inputs((gv==0)?layer_inputs:layer_output_array[gv-1]),
                .layer_outputs(layer_output_array[gv]),
                // Backprop
                .backprop_in_port((gv==0)?backprop_in_port:backprop_ports[gv-1]),
                .backprop_out_port(backprop_ports[gv]),
                .backprop_done(backprop_done_array[gv]),
                .backprop_enable(backprop_enable),
                // Data interface
                .write_enable(write_enable_array[gv]),
                .read_enable(read_enable_array[gv]),
                .write_done(write_done_array[gv]),
                .read_done(read_done_array[gv]),
                .address(translated_address[gv]),
                .data_i(data_i),
                .data_o(data_o_array[gv])
            );
        end
    endgenerate

    assign backprop_out_port = backprop_ports[NUMBER_NEURONS_W-1];
    assign layer_outputs = layer_output_array[NUMBER_NEURONS_W-1];
    assign inference_done = inference_done_array[NUMBER_NEURONS_W-1];
    assign backprop_done = &backprop_done_array;

endmodule
