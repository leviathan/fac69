module layer #(
    parameter NUMBER_SYNAPSES = 8,
    parameter NUMBER_NEURONS = 8,
    parameter IS_RNN = 1
)
(
    clk,
    rst,
    // Turn on processing
    run_inference,
    inference_done,
    // The layer i/o
    layer_inputs,
    layer_outputs,
    // Backprop
    backprop_out_port,
    backprop_in_port,
    backprop_done,
    backprop_enable,
    // Data interface
    write_enable,
    read_enable,
    read_done,
    write_done,
    address,
    data_i,
    data_o
);
    parameter values_neuron = IS_RNN?NUMBER_SYNAPSES+NUMBER_NEURONS+2:NUMBER_SYNAPSES+2;
    parameter total_neuron_values = NUMBER_NEURONS*values_neuron;

    input clk;
    input rst;
    input run_inference;
    output inference_done;
    input[NUMBER_SYNAPSES-1:0] layer_inputs;
    output[NUMBER_NEURONS-1:0] layer_outputs;

    // Backprop
    output[NUMBER_SYNAPSES-1:0] backprop_out_port; // tell the other neurons that they have to better themselves
    input[NUMBER_NEURONS-1:0] backprop_in_port; // Constructive criticism for the neurons
    output backprop_done;
    input backprop_enable;

    // Data interface
    input write_enable;
    input read_enable;
    input [$clog2(total_neuron_values)+1:0] address;
    input [31:0] data_i;
    output reg[31:0] data_o;
    output reg read_done;
    output reg write_done;

    wire[NUMBER_NEURONS-1:0][NUMBER_SYNAPSES-1:0] backprop_out_port_array;
    wire[NUMBER_SYNAPSES-1:0][NUMBER_NEURONS-1:0] backprop_out_port_array_flipped;

    generate
        for(genvar gv1=0;gv1<NUMBER_NEURONS;gv1=gv1+1) begin : flip_outer_loop
            for(genvar gv2=0;gv2<NUMBER_SYNAPSES;gv2=gv2+1) begin : flip_inner_loop
                assign backprop_out_port_array_flipped[gv2][gv1] = backprop_out_port_array[gv1][gv2];
            end
        end
    endgenerate

    generate
        for(genvar gv=0;gv<NUMBER_SYNAPSES;gv=gv+1) begin : backprop_loop
            assign backprop_out_port[gv] = |backprop_out_port_array_flipped[gv];
        end
    endgenerate
    
    wire[NUMBER_NEURONS-1:0] inference_done_array;
    assign inference_done = &inference_done_array;

    wire[NUMBER_NEURONS-1:0] backprop_done_array;
    assign backprop_done = &backprop_done_array;

    wire [NUMBER_NEURONS-1:0][$clog2(values_neuron)+1:0]translated_address;
    generate
        for(genvar gv=0;gv<NUMBER_NEURONS;gv=gv+1) begin : translate_addr
            assign translated_address[gv] = address-gv*values_neuron;
        end
    endgenerate

    wire [NUMBER_NEURONS-1:0] read_enable_array;
    generate
        for(genvar gv=0;gv<NUMBER_NEURONS;gv=gv+1) begin : re_loop
            assign read_enable_array[gv] = (translated_address[gv]<values_neuron) && read_enable;
        end
    endgenerate

    wire [NUMBER_NEURONS-1:0] write_enable_array;
    generate
        for(genvar gv=0;gv<NUMBER_NEURONS;gv=gv+1) begin : we_loop
            assign write_enable_array[gv] = (translated_address[gv]<values_neuron) && write_enable;
        end
    endgenerate

    wire [NUMBER_NEURONS-1:0][31:0] data_o_array;
    wire [NUMBER_NEURONS-1:0] read_done_array;
    wire [NUMBER_NEURONS-1:0] write_done_array;

    always @(posedge clk) begin
        if(rst) begin
            data_o <= 0;
            read_done <= 0;
        end
        else begin
            for (integer i=0; i<NUMBER_NEURONS; i+=1) begin
                if(write_enable_array[i]) begin
                    write_done <= write_done_array[i];
                end
                if(read_enable_array[i]) begin
                    data_o <= data_o_array[i];
                    read_done <= read_done_array[i];
                end
            end
        end
        if(read_done && !read_enable) read_done <= 0;
        if(write_done && !write_enable) write_done <= 0;
    end

    generate
        for(genvar gv=0;gv<NUMBER_NEURONS;gv=gv+1) begin : neurons

            neuron #(
                .NUMBER_SYNAPSES(IS_RNN?NUMBER_SYNAPSES+NUMBER_NEURONS:NUMBER_SYNAPSES)
            )
            ut(
                .clk(clk),
                .rst(rst),
                // Enable inference
                .run_inference(run_inference),
                .inference_done(inference_done_array[gv]),
                // The data I/O
                .neuron_inputs(IS_RNN?{layer_inputs,layer_outputs}:layer_inputs),
                .neuron_output(layer_outputs[gv]),
                // Backprop
                .backprop_out_port(backprop_out_port_array[gv]),
                .backprop_in_port(backprop_in_port[gv]),
                .backprop_done(backprop_done_array[gv]),
                .backprop_enable(backprop_enable),
                // Data interface
                .write_enable(write_enable_array[gv] ),
                .read_enable(read_enable_array[gv]),
                .write_done(write_done_array[gv]),
                .read_done(read_done_array[gv]),
                .address(translated_address[gv]),
                .data_i(data_i),
                .data_o(data_o_array[gv])
            );

        end
    endgenerate

endmodule
