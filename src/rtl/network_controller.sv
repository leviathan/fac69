// Addresses:
`define REGISTER_COMMAND      8'b0000
`define REGISTER_VALUE1       8'b0100
`define REGISTER_VALUE2       8'b1000

// Commands:
`define ACCESS_PARAMS 1
`define RUN_INFERENCE 2
`define RUN_TRAINING 3
`define GENERATE_RANDOM_NUMBER 4
`define RESET_NETWORK 5

// Components:
`define COMPONENT_ENCODER 8'h1
`define COMPONENT_HIDDEN 8'h2
`define COMPONENT_DECODER 8'h3

// Inference commands
`define INFERENCE_OPTION_START 1
`define INFERENCE_OPTION_FETCH 2

// Training commands
`define TRAINING_SINGLE_SHOT 1

module network_controller #(
    parameter NUMBER_SYNAPSES = 8,
    parameter NUMBER_INPUT_NEURONS = 8,
    parameter NUMBER_OUTPUT_NEURONS = 8,
    parameter NUMBER_HIDDEN_NEURONS_W = 8,
    parameter NUMBER_HIDDEN_NEURONS_H = 8
)
(
    clk,
    rst,
    rst_ann,
    // Turn on processing
    run_inference,
    decoder_inference_done,
    encoder_inference_done,
    // DMA interface
    mem_enable,
    mem_wstrb,
    mem_addr,
    mem_data_i,
    mem_data_o,
    mem_ready,
    // The layer i/o
    encoder_input,
    encoder_output,
    decoder_output,
    // The backprop port
    backprop_port,
    backprop_done,
    backprop_enable,
    // Network control signals
    //   Encoder:
    encoder_addr,
    encoder_data_i,
    encoder_data_o,
    encoder_we,
    encoder_read_enable,
    encoder_read_done,
    encoder_write_done,
    //   Hidden:
    hidden_addr,
    hidden_data_i,
    hidden_data_o,
    hidden_we,
    hidden_read_enable,
    hidden_read_done,
    hidden_write_done,
    //   Decoder:
    decoder_addr,
    decoder_data_i,
    decoder_data_o,
    decoder_we,
    decoder_read_enable,
    decoder_read_done,
    decoder_write_done
);
    input clk;
    input rst;
    output reg rst_ann;

    // Turn on processing
    output reg run_inference;
    input decoder_inference_done;
    input encoder_inference_done;

    // DMA interface
    input mem_enable;
    input [3:0]  mem_wstrb;
    input [7:0] mem_addr;
    input [31:0] mem_data_i;
    output reg[31:0] mem_data_o;
    output reg mem_ready;

    // The layer i/o
    output reg[NUMBER_SYNAPSES-1:0] encoder_input;
    input[NUMBER_INPUT_NEURONS-1:0] encoder_output;
    input[NUMBER_OUTPUT_NEURONS-1:0] decoder_output;
    
    // Training
     // tell the other neurons that they have to better themselves:
    output reg[NUMBER_OUTPUT_NEURONS-1:0] backprop_port;
    input backprop_done;
    output reg backprop_enable;

    // Network control signals
    wire[3:0][7:0] bytes_i;
    wire wr_enable;
    genvar gv;

    //   Encoder:
    output reg [31:0] encoder_addr;
    input [31:0] encoder_data_i;
    output reg [31:0] encoder_data_o;
    output reg encoder_we;
    output reg encoder_read_enable;
    input encoder_read_done;
    input encoder_write_done;

    //   Hidden:
    output reg [31:0] hidden_addr;
    input [31:0] hidden_data_i;
    output reg [31:0] hidden_data_o;
    output reg hidden_we;
    output reg hidden_read_enable;
    input hidden_read_done;
    input hidden_write_done;

    //   Decoder:
    output reg [31:0] decoder_addr;
    input [31:0] decoder_data_i;
    output reg [31:0] decoder_data_o;
    output reg decoder_we;
    output reg decoder_read_enable;
    input decoder_read_done;
    input decoder_write_done;
    
    // Access engine
    reg [7:0] command;
    reg [7:0] option;

    // Random number generator
    wire [31:0] lfsr_data;
    lfsr random_numbers(
        .clk(clk),
        .rst(rst),
        .op(lfsr_data)
    );

    // Registers for timing
    reg read_done;

    // Map to bytes:
    generate
        for(gv=0; gv<4; gv=gv+1) begin : map_mem_bytes
            assign bytes_i[gv] = mem_data_i[(gv+1)*8-1:gv*8];
        end
    endgenerate
    
    assign wr_enable = &mem_wstrb;

    always @(posedge clk) begin
        if (rst) begin
            mem_data_o <= 0;
            mem_ready <= 0;
            encoder_we <= 0;
            encoder_data_o <= 0;
            hidden_we <= 0;
            hidden_data_o <= 0;
            command <= 0;
            option <= 0;
            backprop_port <= 0;
            encoder_input <= 0;
            run_inference <= 0;
            backprop_enable <= 0;
            encoder_read_enable <= 0;
            hidden_read_enable <= 0;
            decoder_read_enable <= 0;
            read_done <= 0;
            rst_ann <= 1;
        end
        else
        if (!mem_ready && mem_enable) begin
            if (wr_enable && (mem_addr==`REGISTER_COMMAND) && !backprop_enable ) begin
                command <= bytes_i[0];
                option <= bytes_i[1];
                mem_ready <= 1;
            end
            else
            if (command==`RESET_NETWORK) begin
                rst_ann <= 1; // Resetting the network
                command <= 0;
            end
            else
            if (wr_enable && (mem_addr==`REGISTER_VALUE1) && (command==`ACCESS_PARAMS) && (option==`COMPONENT_ENCODER) ) begin
                encoder_addr <= mem_data_i;
                mem_ready <= 1;
            end
            else
            if (wr_enable && (mem_addr==`REGISTER_VALUE1) && (command==`ACCESS_PARAMS) && (option==`COMPONENT_HIDDEN) ) begin
                hidden_addr <= mem_data_i;
                mem_ready <= 1;
            end
            else
            if (wr_enable && (mem_addr==`REGISTER_VALUE1) && (command==`ACCESS_PARAMS) && (option==`COMPONENT_DECODER) ) begin
                decoder_addr <= mem_data_i;
                mem_ready <= 1;
            end
            else
            if (wr_enable && (mem_addr==`REGISTER_VALUE1) && (command==`RUN_INFERENCE) && (option==`INFERENCE_OPTION_START)  ) begin
                encoder_input <= mem_data_i[NUMBER_SYNAPSES-1:0];
                mem_ready <= 1;
                run_inference <= 1;
            end
            else
            if (wr_enable && (mem_addr==`REGISTER_VALUE1) && (command==`RUN_TRAINING) && (option==`TRAINING_SINGLE_SHOT)  ) begin
                backprop_port <= mem_data_i[NUMBER_OUTPUT_NEURONS-1:0];
                mem_ready <= 1;
                backprop_enable <= 1;
            end
            else
            if ((mem_addr==`REGISTER_VALUE2) && (command==`ACCESS_PARAMS) && (option==`COMPONENT_ENCODER) ) begin
                if(wr_enable) begin
                    encoder_data_o <= mem_data_i;
                    encoder_we <= 1;
                    if(encoder_write_done) begin
                        mem_ready <= 1;
                    end
                end
                else begin              
                    if(!encoder_read_enable && !read_done) begin
                        encoder_read_enable <= 1;
                    end
                    else
                    if(encoder_read_enable && !read_done) begin
                        read_done <= encoder_read_done;
                    end
                    else begin
                        mem_data_o <= encoder_data_i;
                        read_done <= 0;
                        mem_ready <= 1;
                        encoder_read_enable <= 0;
                    end
                end  
            end
            else
            if ((mem_addr==`REGISTER_VALUE2) && (command==`ACCESS_PARAMS) && (option==`COMPONENT_HIDDEN) ) begin
                if(wr_enable) begin
                    hidden_data_o <= mem_data_i;
                    hidden_we <= 1;
                    if(hidden_write_done) begin
                        mem_ready <= 1;
                    end
                end
                else begin              
                    if(!hidden_read_enable && !read_done) begin
                        hidden_read_enable <= 1;
                    end
                    else
                    if(hidden_read_enable && !read_done) begin
                        read_done <= hidden_read_done;
                    end
                    else begin
                        mem_data_o <= hidden_data_i;
                        read_done <= 0;
                        mem_ready <= 1;
                        hidden_read_enable <= 0;
                    end
                end 
            end
            else
            if ((mem_addr==`REGISTER_VALUE2) && (command==`ACCESS_PARAMS) && (option==`COMPONENT_DECODER) ) begin
                if(wr_enable) begin
                    decoder_data_o <= mem_data_i;
                    decoder_we <= 1;
                    if(decoder_write_done) begin
                        mem_ready <= 1;
                    end
                end
                else begin              
                    if(!decoder_read_enable && !read_done) begin
                        decoder_read_enable <= 1;
                    end
                    else
                    if(decoder_read_enable && !read_done) begin
                        read_done <= decoder_read_done;
                    end
                    else begin
                        mem_data_o <= decoder_data_i;
                        read_done <= 0;
                        mem_ready <= 1;
                        decoder_read_enable <= 0;
                    end
                end 
            end
            else
            if ((mem_addr==`REGISTER_VALUE1) && (command==`RUN_INFERENCE) && (option==`INFERENCE_OPTION_FETCH) ) begin
                if(decoder_inference_done) begin
                    run_inference <= 0;
                    mem_data_o <= decoder_output;
                    mem_ready <= 1;
                end
            end
            else
            if ((mem_addr==`REGISTER_VALUE1) && (command==`GENERATE_RANDOM_NUMBER) ) begin
                mem_data_o <= lfsr_data;
                mem_ready <= 1;
            end
        end

        // House keeping
        if(mem_ready && !mem_enable) mem_ready <= 0;
        if(!rst && rst_ann) rst_ann <= 0;
        if(backprop_enable && backprop_done ) begin
            backprop_enable <= 0;
            command <= 0;
        end
        if(encoder_we && !wr_enable) begin
            encoder_we <= 0;
        end
        if(hidden_we && !wr_enable) begin
            hidden_we <= 0;
        end
        if(decoder_we && !wr_enable) begin
            decoder_we <= 0;
        end

    end

endmodule
