#include "params.h"

#include "telnet_server.hpp"

#include <cstdio>
#include <cmath>
#include <iostream>
#include <random>

#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include "Vecp5_minifpga.h"

#include "verilated.h"
#include "verilated_vcd_c.h"

#define ACK 06
#define SYN 22

#define TRACE 1

#ifndef TRACE
#define TRACE 0
#endif

#if TRACE
static VerilatedVcdC *trace;
#endif

// Declare the global testbench, of a type externally defined
typedef Vecp5_minifpga testbench;
static testbench *tb;

static double timestamp = 0;

void tick()
{
    tb->clk = 0;
    tb->eval();
#if TRACE
    trace->dump(timestamp);
#endif
    timestamp += 1;
    tb->clk = 1;
    tb->eval();
#if TRACE
    trace->dump(timestamp);
#endif
    timestamp += 1;
}

void send_uart_char(char ci)
{
    uint8_t c = (uint8_t)ci;
    uint16_t ser_period = 106;
    for(int i=0; i<ser_period; i++) tick();
    tb->ser_rx=0;
    for(int i=0; i<ser_period; i++) tick();
    for(int j=0; j<8; j++) {
        /* Recv:
         * charbuf = (charbuf >> 1);
         * charbuf |= ((tb->ser_tx) & 1) << 7;
         */
        tb->ser_rx=(c>>j)&1;
        for(int i=0; i<ser_period; i++) tick();
    }
    tb->ser_rx=1;
    for(int i=0; i<ser_period; i++) tick();
}

int get_uart_char(char *c)
{
    bool last_ser_tx = false;
    bool curr_ser_tx = false;
    uint8_t charbuf = 0;
    uint16_t ser_half_period = 53;
    last_ser_tx = (tb->ser_tx) & 1;
    tick();
    curr_ser_tx = (tb->ser_tx) & 1;
    if(!curr_ser_tx && last_ser_tx) {
        for(int i=0; i<2*ser_half_period; i++) tick();
        for(int j=0; j<8; j++) {
            for(int i=0; i<ser_half_period; i++) tick();
            charbuf = (charbuf >> 1);
            charbuf |= ((tb->ser_tx) & 1) << 7;
            for(int i=0; i<ser_half_period; i++) tick();
        }
        *c = (char)charbuf;
        return 0;
    }
    return 1;
}

void syn()
{
    char charbuf = 0;
    send_uart_char(SYN);
    while(true) {
        while(get_uart_char(&charbuf)) {}
        if(charbuf==ACK) break;
    }
}

void ack()
{
    char charbuf = 0;
    while(true) {
        while(get_uart_char(&charbuf)) {}
        if(charbuf==SYN) break;
    }
    send_uart_char(ACK);
}

void send_uart_string(std::string message)
{
    for (char & c : message) {
        syn();
        send_uart_char(c);
    }
    syn();
    send_uart_char(0);
}

std::string get_response()
{
    char charbuf=0; 
    std::string stringbuf = "";
    int i=0;
    while(true) {
        ack();
        while(get_uart_char(&charbuf)) {}
        syn();
        if(!charbuf) break;
        stringbuf.push_back(charbuf);
    }
    return stringbuf;
}

int main(int argc, char **argv)
{

    // Initialize Verilators variables
    Verilated::commandArgs(argc, argv);
#if TRACE
    Verilated::traceEverOn(true);
    trace = new VerilatedVcdC;
#else
    Verilated::traceEverOn(false);
#endif

    // Buffer for bits
    char charbuf;

    // Create an instance of our module under test
    tb = new testbench;
#if TRACE
    tb->trace(trace, 99);
    trace->open("ecp5_minifpga.vcd");
#endif

    tb->ser_rx=1;
    tb->leds=0;

    std::cout << "Being simulated" << std::endl;
    //boost::thread thrd(&simulation_thread);

    boost::asio::io_service io_service;
    TelnetServer ts(io_service, 5021);
    io_service.run();

    std::cout <<  std::endl;
    std::cout << "End simulation" << std::endl;

#if TRACE
    trace->close();
#endif
    delete tb;
    return(0);
}
