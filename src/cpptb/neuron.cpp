#include <cstdio>
#include <cmath>
#include <iostream>
#include <random>

#include "Vneuron.h"

#include "verilated.h"
#include "verilated_vcd_c.h"

static VerilatedVcdC *trace;

// Declare the global testbench, of a type externally defined
typedef Vneuron testbench;
static testbench *tb;

static double timestamp = 0;

bool randomBool() {
    static auto gen = std::bind(std::uniform_int_distribution<>(0,1),std::default_random_engine());
    return gen();
}

void tick() {
    tb->clk = 1;
    tb->eval();
    trace->dump(timestamp);
    timestamp += 1;
    tb->clk = 0;
    tb->eval();
    trace->dump(timestamp);
    timestamp += 1;
}

int main(int argc, char **argv) {

    // Initialize Verilators variables
    Verilated::commandArgs(argc, argv);
    Verilated::traceEverOn(true);
    trace = new VerilatedVcdC;

    // Create an instance of our module under test
    tb = new testbench;
    tb->trace(trace, 99);
    trace->open("neuron.vcd");

    // Reset the testbench
    tick();
    tb->rst = 0;
    tick();
    tb->rst = 1;
    tick();
    tb->rst = 0;
    tick();

    int outreg = 0;
    tb->serial_enable = 1;
    tb->serial_in = 1;
    outreg = tb->serial_out;
    tick();
    while(!outreg) {
        tb->serial_in = randomBool();
        outreg = tb->serial_out;
        tick();
    }
    tb->serial_enable = 0;

    for(int i=0; i<100; i++) {
        tb->neuron_inputs = i;
        std::cout << "neuron_inputs: " << i << std::endl;
        for(int j=0; j<200; j++) tick();
        std::cout << "neuron_output: " << tb->neuron_output << std::endl;
    }

    trace->close();
    delete tb;
    return(0);
}
