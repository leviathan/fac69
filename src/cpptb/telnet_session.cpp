#include "telnet_session.hpp"

#include "telnet_server.hpp"

#include <iostream>

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>

namespace ba = boost::asio;
namespace bs = boost::system;
using boost::asio::ip::tcp;

extern void send_uart_string(std::string message);
extern std::string get_response();
extern void syn();
extern void ack();

TelnetSession::TelnetSession(ba::io_service& io_service)
    : mSocket(io_service)
{
}

void TelnetSession::start()
{
    mSocket.async_read_some(
        boost::asio::buffer(data, max_length),
        boost::bind(
            &TelnetSession::handle_read,
            this,
            ba::placeholders::error,
            ba::placeholders::bytes_transferred
        )
    );
}

void TelnetSession::handle_read(const bs::error_code& error, size_t bytes_transferred)
{
    std::string response;
    std::string message;
    if (!error)
    {
        message = std::string(data, bytes_transferred);
        if(message=="TERMINATE") {
            mSocket.close();
        }
        ack();
        syn();
        send_uart_string(message);
        ack();
        syn();
        response = get_response();
        ba::async_write(mSocket,
            ba::buffer(response), 
            boost::bind(&TelnetSession::handle_write, this, ba::placeholders::error));
    }
    else
    {
        delete this;
    }
}

void TelnetSession::handle_write(const bs::error_code& error)
{
    if (!error)
    {
        mSocket.async_read_some(
            ba::buffer(data,max_length),
            boost::bind(
                &TelnetSession::handle_read,
                this,
                ba::placeholders::error,
                ba::placeholders::bytes_transferred
            )
        );
    }
    else
    {
        delete this;
    }
}
