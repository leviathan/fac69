#include <cstdio>
#include <cmath>
#include <iostream>
#include <random>
#include <vector>

#define FMT_HEADER_ONLY
#include <fmt/format.h>

#include "Vnetwork.h"

#include "verilated.h"
#include "verilated_vcd_c.h"

#define TOTAL_NEURONS NUM_INPUT_NEURONS

/* Weights and biases memory of one neuron:
 * Each RNN input is one neuron output, plus 16 bit token space
 * and one additional byte for the bias
 * 
 */
#define MEM_SIZE_NEURON (NUM_INPUT_SYNAPSES+TOTAL_NEURONS+1)

/* Total memory is the amount of neurons multiplied
 * by the memory of one neuron (perceptron)
 * (duh)
 */
//#define MEM_SIZE MEM_SIZE_NEURON*TOTAL_NEURONS
#define MEM_SIZE 500

static VerilatedVcdC *trace;

// Declare the global testbench, of a type externally defined
typedef Vnetwork testbench;
static testbench *tb;

static double timestamp = 0;

bool randomBool() {
    static auto gen = std::bind(std::uniform_int_distribution<>(0,1),std::default_random_engine());
    return gen();
}

void tick() {
    tb->clk = 1;
    tb->eval();
    trace->dump(timestamp);
    timestamp += 1;
    tb->clk = 0;
    tb->eval();
    trace->dump(timestamp);
    timestamp += 1;
}

int run_inference(std::vector<uint16_t> tokens) {
    int output;
    //tb->layer_inputs = prompt;
    for(size_t i=0; i<tokens.size()/2; i++) {
        tb->layer_inputs[i]=0;
        tb->layer_inputs[i]|=tokens.at(i*2);
        tb->layer_inputs[i]|=tokens.at(i*2+1)<<16;
        tick();
    }
    tick();
    tick();
    tick();
    tick();
    tick();

    for(int i=0; i<3000; i++) {
        tick();
        output = tb->layer_outputs;
        if(output) break;
    }
    /*output = 0;
    while(!output) {
        tick();
        output = tb->layer_outputs;
        if(output) break;
    }*/

    //return output;
    return 0;
}

std::vector<uint8_t> generate_initial_memory() {
    std::vector< uint8_t > bytes;
    for(uint8_t i=0; i<MEM_SIZE; i++) {
        bytes.push_back(i+1);
    }
    return bytes;
}

void write_weights(std::vector<uint8_t> params) {
    tb->serial_enable = 1;
    for(const uint8_t& i : params) {
        // std::cout << "Write value: " << std::to_string(i) << std::endl;
        for(int x=0; x<8; x++) {
            tb->serial_in = (i>>x) & 1;
            // std::cout << "Bit: " << std::to_string(tb->serial_in) << std::endl;
            tick();
        }
    }
    tb->serial_enable = 0;
    tick();
}

std::vector< uint8_t > read_weights() {
    std::vector< uint8_t > arr;
    uint8_t outreg;
    tb->serial_enable = 1;
    tb->serial_in = 0;
    for(int i=0; i<MEM_SIZE; i++) {
        outreg = 0;
        for(int j=0; j<8; j++) {
            outreg=(outreg<<1)+tb->serial_out;
            tick();
        }
        arr.push_back(outreg);
    }
    tb->serial_enable = 0;
    return arr;
}

/*void write_weights(std::vector< uint8_t > weights) {
    std::vector< uint8_t > arr;
    uint8_t outreg;
    tb->serial_enable = 1;
    tb->serial_in = 0;
    for(int i=0; i<NUM_BYES; i++) {
        outreg = 0;
        for(int j=0; j<8; j++) {
            outreg=(outreg<<1)+tb->serial_out;
            tick();
        }
        arr.push_back(outreg);
    }
    tb->serial_enable = 0;
    return arr;
}*/

void reset_tb() {
    // Reset the testbench
    tick();
    tb->rst = 0;
    tick();
    tb->rst = 1;
    tick();
    tb->rst = 0;
    tick();
}

int main(int argc, char **argv) {

    // Initialize Verilators variables
    Verilated::commandArgs(argc, argv);
    Verilated::traceEverOn(true);
    trace = new VerilatedVcdC;

    // Create an instance of our module under test
    tb = new testbench;
    tb->trace(trace, 99);
    trace->open("layer.vcd");

    reset_tb();

    std::vector<uint8_t> params = generate_initial_memory();
    write_weights(params);

    std::vector<uint16_t> tokens;
    tokens.push_back(27017);
    tokens.push_back(341);
    tokens.push_back(318);
    tokens.push_back(12402);

    run_inference(tokens);

    /*std::vector< uint8_t > vi = read_weights();

    int count = 0;
    uint8_t lasti=0;
    for(const uint8_t& i : vi) {
        lasti = i;
        std::cout << "i = " << std::hex << i << std::endl;
        count++;
        if(!i && !lasti) break;
    }
    std::cout << "count = " << count << std::endl;
    */

    /*int output = 0;
    std::cout << "Inference";
    for(uint8_t i=1; i<32; i++) {
        output = run_inference(i);
        std::cout << "layer_inputs: " << (char)(i+'a') << std::endl;
        std::cout << "layer_outputs: " << (char)(output+'a') << std::endl;
    }*/

    trace->close();
    delete tb;
    return(0);
}
