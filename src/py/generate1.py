from transformers import AutoTokenizer
tokenizer = AutoTokenizer.from_pretrained("gpt2")
prompt = "Taxation is theft"
input_ids = tokenizer(prompt, return_tensors="pt").input_ids
print(input_ids)
