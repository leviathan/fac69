import json

from fac_tools import run_command
from fac_tools import get_fac_wrapper
from fac_tools import load_weights_and_biases
from fac_tools import dump_neural_network
from fac_tools import init_weights_and_biases
from fac_tools import train_token_series
from fac_tools import set_decay_rate
from fac_tools import set_learning_rate
from fac_tools import predict_next_token
from fac_tools import reset_states

#from transformers import AutoTokenizer

import matplotlib.pyplot as plt
import numpy as np

#tokenizer = AutoTokenizer.from_pretrained("gpt2")
#prompt = "Taxation is theft"
#input_ids = tokenizer(prompt, return_tensors="pt").input_ids
#tokens = input_ids[0].numpy()
tokens = [27017, 341, 318, 12402] # Taxation is theft
print(tokens)

server = get_fac_wrapper("telnet")

run_command(server,"HELLO")

# Initialize the values
init_weights_and_biases(server)

# The decay rate is a hyper parameter
# Because delta W aka alpha is calculated
# as the initial learning rate α0 multiplied
# as shown below anything floaty doesn't make sense
# anyway. Usually the decay rate is set to 1
# α=(1/(1+decayRate×epochNumber))*​α0
set_decay_rate(server, 1)

max_epochs=1000

set_decay_rate(server, 1)

errors=[]

# Upload and train token pairs first

for i in range(10):
    reset_states(server)

    set_learning_rate(server, 0.95)
    errors += train_token_series(server, tokens[0:2], max_epochs)
    run_command(server,"DONE")

'''
    reset_states(server)

    set_learning_rate(server, 0.95)
    errors += train_token_series(server, tokens[1:3], max_epochs)
    run_command(server,"DONE")

    reset_states(server)

    set_learning_rate(server, 0.95)
    errors += train_token_series(server, tokens[2:4], max_epochs)
    run_command(server,"DONE")

    set_learning_rate(server, 0.80)
    errors += train_token_series(server, tokens[0:3], max_epochs)
    run_command(server,"DONE")

    set_learning_rate(server, 0.80)
    errors += train_token_series(server, tokens[0:4], max_epochs)
    run_command(server,"DONE")
'''


reset_states(server)

print("Input Token: ", hex(tokens[0]))
tok = predict_next_token(server,tokens[0])
print("Output Token: ", hex(tok))
print("Expected Token: ", hex(tokens[1]))

reset_states(server)

print("Input Token: ", hex(tokens[1]))
tok = predict_next_token(server,tokens[1])
print("Output Token: ", hex(tok))
print("Expected Token: ", hex(tokens[2]))

xaxis = np.array(range(len(errors)))
yaxis = np.array(errors)
plt.plot(xaxis, yaxis)
plt.show()

# Store the weights and biases
weights_and_biases = dump_neural_network(server)
j = json.dumps(weights_and_biases, indent=4, sort_keys=True)
print(j)

run_command(server,"TERMINATE")

server.close()

print("Writing out JSON")
with open("test_files/weights_and_biases_trained.json", "w") as f:
    f.write(j)
    f.close()
