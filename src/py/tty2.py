import json

from fac_tools import run_command
from fac_tools import get_fac_wrapper
from fac_tools import load_weights_and_biases

server = get_fac_wrapper("telnet")

print("Reading in JSON")
with open("test_files/weights_and_biases.json", "r") as f:
    weights_and_biases = json.loads(f.read())
    f.close()

load_weights_and_biases(server, weights_and_biases)

run_command(server,"TERMINATE")

server.close()
