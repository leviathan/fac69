import json

from fac_tools import run_command
from fac_tools import get_fac_wrapper
from fac_tools import dump_neural_network
from fac_tools import init_weights_and_biases

server = get_fac_wrapper("telnet")


# Initialize the values
init_weights_and_biases(server)

weights_and_biases = dump_neural_network(server)

j = json.dumps(weights_and_biases, indent=4, sort_keys=True)
print(j)

run_command(server,"TERMINATE")

server.close()

print("Writing out JSON")
with open("result/weights_and_biases.json", "w") as f:
    f.write(j)
    f.close()
