import sys 

from codecs import decode
from socket import *

class FacTelnetWrapper:
    server = None

    def __init__(self):
        HOST = 'localhost'
        PORT = 5021
        ADDRESS = (HOST, PORT)
        self.server = socket(AF_INET, SOCK_STREAM)
        self.server.connect(ADDRESS)

    def send(self,cmd):
        self.server.send(cmd)
    
    def recv(self,bufsize):
        return self.server.recv(bufsize)

    def close(self):
        self.server.close()

    def wrapper_type(self):
        return "telnet"

def get_fac_wrapper(wrapper_type):
    if wrapper_type == "telnet":
        return FacTelnetWrapper()

def run_command(server, cmd):
    if server.wrapper_type() == "telnet":
        BUFSIZE = 2048
        cmd = cmd.encode("ascii")
        server.send(cmd)
        response = decode(server.recv(BUFSIZE), 'ascii')
        #print("commands: ",cmd," response: ",response)
        sys.stdout.flush() 
        return response

def get_weight(server, layer, wtype, nidx, vidx):
    run_command(server,"READ")
    run_command(server,layer)
    run_command(server,str(nidx))
    run_command(server,wtype)
    ret = run_command(server,str(vidx))
    run_command(server,"DONE")
    return ret

def extract_values(server, layer):
    arr=[]
    i=0
    while True:
        retd={}

        run_command(server,"READ")
        run_command(server,layer)
        ret = run_command(server,str(i))
        if "END" in ret:
            break        
        retd['BIAS'] = int(run_command(server,"BIAS"))
        run_command(server,"DONE")

        run_command(server,"READ")
        run_command(server,layer)
        ret = run_command(server,str(i))
        if "END" in ret:
            break
        j=0
        sarr=[]
        while True:
            ret = run_command(server,str(j))
            if "END" in ret:
                break
            sarr.append(int(ret))
            j=j+1

        retd['WEIGHTS']=sarr
        run_command(server,"DONE")

        arr.append(retd)
        i=i+1

    return arr

def extract_single_value(server, layer, wtype):
    arr = []
    i=0
    while True:
        run_command(server,"READ")
        run_command(server,layer)
        if "END" in run_command(server,str(i)):
            break
        i=i+1
        ret = run_command(server,wtype)
        arr.append(int(ret))
        run_command(server,"DONE")
    return arr

def dump_neural_network(server):
    weights_and_biases = {}
    print("Reading ENCODER")
    weights_and_biases['ENCODER'] = extract_values(server,"ENCODER")
    print("Reading HIDDEN")
    weights_and_biases['HIDDEN'] = extract_values(server,"HIDDEN")
    print("Reading DECODER")
    weights_and_biases['DECODER'] = extract_values(server,"DECODER")
    return weights_and_biases

def write_neuron(server, layer_name, nidx, neuron):
    # Write bias
    run_command(server,"WRITE")
    run_command(server,layer_name)
    run_command(server,str(nidx))
    run_command(server,"BIAS")
    run_command(server,str(neuron["BIAS"]))
    run_command(server,"DONE")
    # Write weights
    run_command(server,"WRITE")
    run_command(server,layer_name)
    run_command(server,str(nidx))
    run_command(server,"WEIGHTS")
    for w in neuron["WEIGHTS"]:
        run_command(server,str(w))
    run_command(server,"DONE")

def write_layer(server, layer_name, layer):
    nidx=0
    for neuron in layer:
        write_neuron(server, layer_name, nidx, neuron)
        nidx=nidx+1

def load_weights_and_biases(server,weights_and_biases):
    print("Loeading ENCODER")
    write_layer(server, "ENCODER", weights_and_biases['ENCODER'])
    print("Loeading HIDDEN")
    write_layer(server, "HIDDEN", weights_and_biases['HIDDEN'])
    print("Loeading DECODER")
    write_layer(server, "DECODER", weights_and_biases['DECODER'])

def init_weights_and_biases(server):
    run_command(server,"INIT")
    run_command(server,"WEIGHTS")
    run_command(server,"INIT")
    run_command(server,"BIAS")

def train_token_series(server, tokens, epochs):
    arr = []
    run_command(server,"TRAIN")
    run_command(server,"TOKENS")
    for tok in tokens:
        run_command(server,str(tok))
    run_command(server,"DONE")

    run_command(server,"TRAIN")
    run_command(server,"RUN_SINGLE_EPOCH")
    for i in range(0,epochs):
        print("Training epoch: ",i)
        ret = run_command(server,str(i))
        arr.append(int(ret))
        print("Error: ", ret)
        if "0" == ret:
            break
    return arr


# The decay rate is a hyper parameter
# Because delta W aka alpha is calculated
# as the initial learning rate α0 multiplied
# as shown below anything floaty doesn't make sense
# anyway. Usually the decay rate is set to 1
# α=(1/(1+decayRate×epochNumber))*​α0
def set_decay_rate(server, rate):
    run_command(server,"TRAIN")
    run_command(server,"DECAY_RATE")
    run_command(server,str(rate))


# PicoRV does not have a floating point unit
# All weights are stored as signed 32 bit integers
# this means we have to do the math here and multiply
# the maximum integer value by our desired learning rate

INTMAX=2147483647
def set_learning_rate(server, learning_rate):
    lr=int(learning_rate*INTMAX)
    run_command(server,"TRAIN")
    run_command(server,"LEARNING_RATE")
    run_command(server,str(lr))

def predict_next_token(server, token):
    run_command(server,"PREDICT")
    ret = run_command(server,str(token))
    return int(ret)

def reset_states(server):
    run_command(server,"RESET")

